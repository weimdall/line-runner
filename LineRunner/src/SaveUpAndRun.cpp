/**
 * \file SaveUpAndRun.cpp
 * \brief Gestion du jeu
 * \author J.Cassagne
 */

#include "SaveUpAndRun.h"

SaveUpAndRun::SaveUpAndRun()
{
    #ifdef DEBUG
        debug = true;
        app = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Line Runner",
    sf::Style::Fullscreen);
    #else
        debug = false;
        app = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Line Runner", sf::Style::Fullscreen);
    #endif

    stage = new Mapping(app);
    app->setFramerateLimit(60);
    view = app->getDefaultView();

    #ifdef WIN32
    app->setVerticalSyncEnabled(false);
    #else
    app->setMouseCursorVisible(true);
    #endif


    if(!this->DemandePseudo())
        exit(1);

    menu = true;

    if(!this->Charger())
        exit(2);
    serveur = new Serveur(&tblJoueurs, &joueur, stage);
    Boucle();
}

SaveUpAndRun::~SaveUpAndRun()
{
    delete stage;
    for(int i = 0 ; i < NB_ANIMATION-1 ; i++) //Ne pas delete le ACTUEL
        delete joueur.anim[i];
    delete app;
}

bool SaveUpAndRun::DemandePseudo()
{
    sf::Text sfPseudo, titre;
    sf::String pseudo;
    sf::RectangleShape rect;

    rect.setSize(sf::Vector2f(app->getSize().x/1.5f,50));
    rect.setPosition(view.getCenter().x-(rect.getGlobalBounds().width/2), view.getCenter().y-(rect.getGlobalBounds().height/2));

    if (!police.loadFromFile("./data/font.ttf"))
        return false;
    sfPseudo.setFont(police);
    titre.setFont(police);
    titre.setString("Pseudo : ");
    titre.setPosition(rect.getPosition().x, rect.getPosition().y-(titre.getGlobalBounds().height*3));
    titre.setColor(sf::Color::White);
    sfPseudo.setColor(sf::Color::Black);
    sfPseudo.setPosition(rect.getPosition());


    bool continuer = true;
    sf::Event event;
    while (continuer || !app->isOpen())
    {
        while(continuer && app->pollEvent(event))
        {
            if(event.type == sf::Event::TextEntered && pseudo.getSize() < 20)
                pseudo += static_cast<char>(event.text.unicode);
            if (event.key.code == sf::Keyboard::Return && pseudo.getSize() > 0)
                continuer = false;
            if (event.key.code == sf::Keyboard::BackSpace && pseudo.getSize() > 0)
                pseudo.erase(pseudo.getSize()-1);

        }
        sfPseudo.setString(pseudo);
        app->clear(sf::Color(0,0,0));
        app->draw(rect);
        app->draw(titre);
        app->draw(sfPseudo);

        app->display();
    }

    joueur.nom.setString(pseudo);
    while(app->pollEvent(event)){}
    sf::sleep(sf::milliseconds(250));
    return true;

}

bool SaveUpAndRun::Charger()
{
    if(!stage->LireFichier("Stage"))
        return false;
    if (!police.loadFromFile("./data/font.ttf"))
        return false;

    fond.loadFromFile("./data/fond2.png");
    fond.setRepeated(true);
    sprFond.setTexture(fond);
    sprFond.setColor(sf::Color(200,200,200));
    nomMap.setFont(police);

    joueur.anim[COURS] = new Animation("./data/run.anim");
    joueur.anim[TOMBE] = new Animation("./data/tombe.anim");
    joueur.anim[SAUTE] = new Animation("./data/saute.anim");
    joueur.animationActuel = COURS;
    sol = false;
    vitesse = 0;
    joueur.score = 0;
    joueur.nom.setFont(police);

    score.setFont(police);
    score.setCharacterSize(30);

    //Debug
    texteDebug.setFont(police);
    texteDebug.setCharacterSize(20);

    //Tableau scores
    for(int i = 0 ; i < 10 ; i++)
    {
        tblScore[i].r.setOutlineColor(sf::Color::White);
        tblScore[i].r.setOutlineThickness(2.f);
        tblScore[i].r.setSize(sf::Vector2f(1500, 100));
        tblScore[i].r.setFillColor(sf::Color(255,255,255,0));

        tblScore[i].t[0].setFont(police);
        tblScore[i].t[0].setCharacterSize(100);
        tblScore[i].t[1].setFont(police);
        tblScore[i].t[1].setCharacterSize(100);


    }

    return true;
}

void SaveUpAndRun::Menu()
{
    static int translationX = 0;
    if(clock.getElapsedTime().asMilliseconds() > 10)
    {
        translationX+=2;
        if(translationX > stage->getLongeurStage())
            translationX = 0;
        if(translationX < 0)
            translationX = stage->getLongeurStage();
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            translationX -= 22;
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            translationX += 20;
        clock.restart();
    }
    view.setSize(4800, 2400);
    view.setCenter(sf::Vector2f(translationX, stage->getHauteurStage()/2));
    app->setView(view);
    this->Draw();
}

void SaveUpAndRun::Boucle()
{
    sizeView = view.getSize();
    while (app->isOpen())
    {
        sf::Event event;
        while (app->pollEvent(event))
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q) && menu)
                app->close();
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                joueur.position = sf::Vector2f(0,-200);
                menu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
            {
                Restart();
                menu = false;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
            {
                #ifdef DEBUG
                debug = !debug;
                #endif
            }
        }
        if(menu)
            Menu();
        else
        {
            Gravite(0);
            if(joueur.position.y <= stage->getHauteurStage())
                view.setCenter(joueur.position);
            else
                view.setCenter(joueur.position.x, stage->getHauteurStage());
            view.setSize(sizeView);
            app->setView(view);

            if(joueur.animationActuel != TOMBE)
            {
                if(sol)
                {
                    if(joueur.animationActuel != COURS)
                    {
                        joueur.animationActuel = COURS;
                        joueur.anim[COURS]->Relancer();
                    }
                }
                else
                    joueur.animationActuel = SAUTE;
                if(stage->TestCollision(joueur.anim[joueur.animationActuel]->getSprite(), sf::Vector2f(joueur.position.x-25, joueur.position.y), DROITE))
                    this->Meurt();
                if(stage->Sortie(joueur.position))
                    this->Meurt();
                if(debug)
                {
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
                        joueur.position.x += VITESSE;
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
                        joueur.position.x -= VITESSE;
                    if(sf::Keyboard::isKeyPressed(sf::Keyboard::R))
                        this->Restart();
                }
                else
                    joueur.position.x += VITESSE;
                if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) || sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
                {
                    if(sol)
                    {
                        sol = false;
                        Gravite(JUMP);
                    }
                }
            }
            else
                if(clock.getElapsedTime().asSeconds() >= 1.5)
                    this->Restart();

            this->Draw();
        }

    }
    serveur->Deconnection();
}

void SaveUpAndRun::Draw()
{
    nomMap.setString(stage->getNom());

    nomMap.setPosition(view.getCenter().x-(nomMap.getGlobalBounds().width/2), view.getCenter().y-(view.getSize().y/2));
    joueur.anim[joueur.animationActuel]->getSprite()->setPosition(joueur.position);
    joueur.nom.setPosition(joueur.position.x-(joueur.nom.getGlobalBounds().width/2)+(joueur.anim[joueur.animationActuel]->getSprite()->getGlobalBounds().width/2), joueur.position.y-(joueur.nom.getGlobalBounds().height*2));

    sprFond.setTextureRect(sf::IntRect(0,0,view.getSize().x, view.getSize().y));
    sprFond.setPosition(view.getCenter().x - view.getSize().x/2, view.getCenter().y - view.getSize().y/2);
    app->clear(sf::Color(153,153,153));
    app->draw(sprFond);
    stage->getMutex()->lock();
    stage->Draw(view);
    stage->getMutex()->unlock();

    for(unsigned int i = 0 ; i < tblJoueurs.size() ; i++)
    {
        tblJoueurs[i].anim[tblJoueurs[i].animationActuel]->getSprite()->setColor(tblJoueurs[i].couleur);
        tblJoueurs[i].anim[tblJoueurs[i].animationActuel]->getSprite()->setPosition(tblJoueurs[i].position);
        tblJoueurs[i].nom.setPosition(tblJoueurs[i].position.x-(tblJoueurs[i].nom.getGlobalBounds().width/2)+(tblJoueurs[i].anim[tblJoueurs[i].animationActuel]->getSprite()->getGlobalBounds().width/2), tblJoueurs[i].position.y-(tblJoueurs[i].nom.getGlobalBounds().height*2));
        tblJoueurs[i].nom.setFont(police);

        app->draw(*tblJoueurs[i].anim[tblJoueurs[i].animationActuel]->getSprite());
        if(menu)
            tblJoueurs[i].nom.setCharacterSize(90);
        else
            tblJoueurs[i].nom.setCharacterSize(30);
        if(tblJoueurs[i].nom.getString() != sf::String("Nameless"))
            app->draw(tblJoueurs[i].nom);
    }
    if(!menu)
    {
        nomMap.setCharacterSize(30);

        app->draw(*joueur.anim[joueur.animationActuel]->getSprite());
        if(joueur.nom.getString() != sf::String("Nameless"))
            app->draw(joueur.nom);

        this->Score();
    }
    else
    {
        nomMap.setCharacterSize(150);
        sf::RectangleShape rect(sf::Vector2f(view.getSize().x/6, view.getSize().y));
        rect.setPosition(sprFond.getPosition());
        rect.setFillColor(sf::Color(0,0,0,100));
        app->draw(rect);
        sf::Text texte;
        texte.setFont(police);
        texte.setCharacterSize(100);
        texte.setPosition(sprFond.getPosition());
        texte.move(0,view.getSize().y/6);
        std::ostringstream oss;
        oss << "Etat:\n";
        if(serveur->getConnecter())
            oss << "En ligne\n\n\t\tPseudo : \n" << joueur.nom.getString().toAnsiString();
        else
            oss << "Hors ligne\n\n\n";
        oss << "\n\n\n\n\n\n\n\nQ: Quiter\nEnter: Jouer\nEscape: Menu";
        texte.setString(oss.str());

        app->draw(texte);

        this->TblScore();
    }
    if(debug && !menu)
        Debug();
    app->draw(nomMap);
    app->display();
}

void SaveUpAndRun::Gravite(float v)
{
    vitesse += v;
    if(!sol)
        vitesse--;
    //anim[SAUTE]->getSprite()->setOrigin(anim[SAUTE]->getSprite()->getOrigin().x + anim[SAUTE]->getSprite()->getGlobalBounds().width/2, anim[SAUTE]->getSprite()->getOrigin().y + anim[SAUTE]->getSprite()->getGlobalBounds().height/2);
    //anim[SAUTE]->getSprite()->rotate(5);

    if(vitesse > 0)
    {
        if(stage->TestCollision(joueur.anim[joueur.animationActuel]->getSprite(), sf::Vector2f(joueur.position.x, joueur.position.y), HAUT))
            vitesse = 0;
    }
    else
    {
        for(int i = 0 ; i >= vitesse ; i--)
        {
            if(stage->TestCollision(joueur.anim[joueur.animationActuel]->getSprite(), sf::Vector2f(joueur.position.x, joueur.position.y-i-20), BAS))
            {
                sol = true;
                joueur.position.y -= i;
                vitesse = 0;
                break;
            }
            else
                sol = false;
        }
    }
    joueur.position.y -= vitesse;
}

void SaveUpAndRun::Debug()
{
    std::ostringstream oss;
    oss << "FPS : " << (int)(1/clock2.restart().asSeconds()) << "\nVitesse : " << vitesse << "\nG : " << ((sol) ? "True" : "False") << "\nPosition : " << view.getCenter().x << " " << view.getCenter().y;
    oss << "\nServeur : " << ((serveur->getConnecter()) ? "Online" : "Hors ligne");
    texteDebug.setString(oss.str());
    texteDebug.setPosition(view.getCenter().x - view.getSize().x/2, view.getCenter().y - view.getSize().y/2);
    app->draw(texteDebug);
}

void SaveUpAndRun::Score()
{
    sf::Uint32 hs = 0;
    sf::Uint32 scoreActuel = 0;
    scoreActuel = (int)((joueur.position.x/stage->getLongeurStage())*100);

    if(tblJoueurs.size() > 0)
        hs = tblJoueurs[0].score;

    if(joueur.score < scoreActuel)
    {
        joueur.score = scoreActuel;
        if(serveur->getConnecter())
            serveur->EnvoiHightScore(joueur.score);
    }
    if(hs < joueur.score)
        hs = joueur.score;

    std::ostringstream oss;
    oss << "Score : " << scoreActuel << "%\n" << "Meilleur score : " << hs << "%";
    score.setString(oss.str());
    score.setPosition((view.getCenter().x + view.getSize().x/2) - score.getGlobalBounds().width-20, view.getCenter().y - view.getSize().y/2);
    app->draw(score);
}

void SaveUpAndRun::TblScore()
{
    bool placer = false; //Le joueur n'est pas comprit dans le tbl, il faut le placer
    unsigned int i = 0 , j = 0;
    std::ostringstream ossNom, ossScore;
    for(j = 0, i = 0 ; i < tblJoueurs.size() && j < 10 ; i++ , j++)
    {
        ossNom.str("");
        ossNom.clear();
        ossScore.str("");
        ossScore.clear();

        sf::Vector2f pos = nomMap.getPosition();
        pos.y += (nomMap.getGlobalBounds().height*3) + (j*tblScore[j].r.getSize().y);
        pos.x += nomMap.getGlobalBounds().width/2;
        pos.x -= tblScore[j].r.getGlobalBounds().width/2;
        tblScore[j].r.setPosition(pos);
        tblScore[j].t[0].setPosition(tblScore[j].r.getPosition());
        tblScore[j].t[1].setPosition(tblScore[j].r.getPosition().x+tblScore[j].r.getSize().x, tblScore[j].r.getPosition().y);


        if(!placer && tblJoueurs[i].score < joueur.score)
        {
            std::string nom(joueur.nom.getString().toAnsiString());
            ossNom << j+1 << ".\t" << nom;

            ossScore << joueur.score << "%";
            tblScore[j].t[0].setColor(sf::Color::White);
            tblScore[j].t[1].setColor(sf::Color::White);
            i--;
            placer = true;
        }
        else
        {
            std::string nom(tblJoueurs[i].nom.getString().toAnsiString());
            ossNom << j+1 << ".\t" << nom;
            ossScore << tblJoueurs[i].score << "%";
            tblScore[j].t[0].setColor(tblJoueurs[i].couleur);
            tblScore[j].t[1].setColor(sf::Color::White);
        }

        tblScore[j].t[0].setString(ossNom.str());
        tblScore[j].t[1].setString(ossScore.str());

        app->draw(tblScore[j].r);
        app->draw(tblScore[j].t[0]);
        app->draw(tblScore[j].t[1]);
    }
    if(j < 9 && !placer)
    {
        sf::Vector2f pos = nomMap.getPosition();
        pos.y += (nomMap.getGlobalBounds().height*3) + (j*tblScore[j].r.getSize().y);
        pos.x += nomMap.getGlobalBounds().width/2;
        pos.x -= tblScore[j].r.getGlobalBounds().width/2;
        tblScore[j].r.setPosition(pos);
        tblScore[j].t[0].setPosition(tblScore[i].r.getPosition());
        tblScore[j].t[1].setPosition(tblScore[j].r.getPosition().x+tblScore[j].r.getSize().x, tblScore[j].r.getPosition().y);

        ossNom.str("");ossNom.clear();
        ossScore.str("");ossScore.clear();
        std::string nom(joueur.nom.getString().toAnsiString());
        ossNom << j+1 << ".\t" << nom;
        ossScore << joueur.score << "%";

        placer = true;
        tblScore[j].t[0].setString(ossNom.str());
        tblScore[j].t[1].setString(ossScore.str());
        tblScore[j].t[0].setColor(sf::Color::White);
        tblScore[j].t[1].setColor(sf::Color::White);

        app->draw(tblScore[j].r);
        app->draw(tblScore[j].t[0]);
        app->draw(tblScore[j].t[1]);
    }
}

void SaveUpAndRun::Restart()
{
    joueur.position = stage->getSpawn();
    joueur.animationActuel = COURS;
}
void SaveUpAndRun::Meurt()
{
    clock.restart();
    joueur.animationActuel = TOMBE;
    joueur.anim[TOMBE]->Relancer();
}

