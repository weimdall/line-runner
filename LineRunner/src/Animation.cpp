/**
 * \file Animation.cpp
 * \brief Gestion des animations
 * \author J.Cassagne
 */


#include "Animation.h"

using namespace sf;
using namespace std;

Animation::Animation(string nomFichier)
{
    chiffreImageActuel = 0;

    if(LireFichier(nomFichier))
        spr.setTexture(image);
    else
        std::cout << "Erreur anim : " << nomFichier << std::endl;
    spr.setTextureRect(sf::IntRect(0,  0,  largeur, hauteur));
    clock = new Clock();
}
void Animation::FinTimer()
{
    clock->restart();
    if(chiffreImageActuel == (nbImages - 1))
    {
        if(repetition)
            chiffreImageActuel = 0;
        else
            fin = true;
    }
    else
        chiffreImageActuel++;
}
Sprite* Animation::getSprite()
{
    float temps = clock->getElapsedTime().asSeconds();
    if(fin)
        return &spr;
    if(temps >= nbSecondes)
        FinTimer();
    if(chiffreImageActuel == 0)
        spr.setTextureRect(sf::IntRect(0,  0,  largeur, hauteur));
    else
        spr.setTextureRect(sf::IntRect(chiffreImageActuel*(largeur + espace),  0,  largeur, hauteur));

    return &spr;
}

sf::Vector2u Animation::getSize()
{
    return Vector2u(spr.getTextureRect().width, spr.getTextureRect().height);
}

bool Animation::FinAnimation()
{
    return fin;
}

bool Animation::LireFichier(std::string nomFichier)
{
    std::string chaine;
    int param[5];
    float paramf[2];
    int RVB[3];
    sf::Image imgTemp;
    std::ifstream fichier(nomFichier.c_str(), std::ios::in);
    if(!fichier)
        return false;
    fichier >> chaine; //On recupere la premi�re ligne
    if(chaine != "Animation-Version-1.0")
        return false;

    fichier >> chaine; //On recupere le chemin vers l'image
    if(!imgTemp.loadFromFile(ANIMATION+chaine))
        return false;

    fichier >> param[0] >> nbSecondes >> param[1] >> param[2] >> param[3] >> param[4] >> paramf[0] >> paramf[1];

    nbImages = param[0];
    if(param[1] == 0)
        repetition = false;
    else
        repetition = true;
    largeur = param[2];
    hauteur = param[3];
    espace = param[4];
    spr.setScale(paramf[0], paramf[1]);

    fichier >> RVB[0] >> RVB[1] >> RVB[2];
    imgTemp.createMaskFromColor(sf::Color(RVB[0], RVB[1], RVB[2]));
    image.loadFromImage(imgTemp);
    image.setSmooth(false);
    image.setRepeated(false);

    fichier.close();

    fin = false;

    return true;
}

void Animation::Relancer()
{
    fin = false;
    chiffreImageActuel = 0;
    clock->restart();
}

void Animation::setImageActuelle(int img)
{
    if((img >= 0) && (img < nbImages))
    {
        chiffreImageActuel = img;
        clock->restart();
    }
}

void Animation::Stop()
{
    fin = true;
    chiffreImageActuel = nbImages-1;
}
