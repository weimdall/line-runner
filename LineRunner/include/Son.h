/**
 * \file Son.h
 * \brief Gestion du Son
 * \author J.Cassagne
 */


#ifndef SON_H
#define SON_H

#include <SFML/Audio.hpp>
#include <string>
#define SON "data/"



class Son
{
    public:
        Son(std::string chemin, bool boucle);
        void Lire();
        void Pause();
        void Stop();
        void setVolume(float volume);
        bool enLecture();
        void setSon(std::string chemin, bool boucle);
    private:
        sf::Music m_son;


};

#endif // SON_H
