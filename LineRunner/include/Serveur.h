/**
 * \file Serveur.h
 * \brief Gestion du serveur
 * \author J.Cassagne
 */

#ifndef SERVEUR_H
#define SERVEUR_H

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <fstream>
#include <vector>
#include "Animation.h"
#include "Mapping.h"

#define NB_ANIMATION 3
#define PORT 5555

enum Anim
{
    COURS, SAUTE, TOMBE
};

struct Joueur
{
    sf::Uint32 ID;
    sf::Text nom;
    sf::Color couleur;
    sf::Vector2f position;
    Animation* anim[NB_ANIMATION];
    sf::Uint16 animationActuel;
    sf::Uint32 score;
};

class Serveur
{
    public:
        Serveur(std::vector<Joueur> *tbl, Joueur *j, Mapping *m);
        virtual ~Serveur();
        bool Connection();
        void Deconnection();
        void TraitementPaquet();
        void EnvoiPaquet();
        bool getConnecter();
        unsigned int getNbJoueurs();
        void EnvoiHightScore(int hs);

        static void OrdonneTbl(std::vector<Joueur> &tbl);


    private:
        sf::Thread *thread1, *thread2;
        std::vector<Joueur> *tblJoueurs;
        Joueur *joueur;
        sf::TcpSocket serveur;
        Mapping *map;
        bool arret;
        bool connecter;
};

#endif // SERVEUR_H
