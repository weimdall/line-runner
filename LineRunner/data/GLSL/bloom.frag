#ifdef GL_ES
precision mediump float;
#endif

uniform float time;
varying vec2 surfacePosition;

#define MAX_ITER 9
void main( void ) {
	vec2 sp = surfacePosition;//vec2(.4, .7);
	vec2 p = sp*5.0 - vec2(10.0);
	vec2 i = p;
	float c = 1.0;
	
	float inten = 0.01;

	for (int n = 0; n < MAX_ITER; n++) 
	{
		float t = time* (1.0 - (3.0 / float(n+1)));
		i = p + vec2(sin(t - i.x) +sin(t + i.y), cos(time - i.y)+cos(t + i.x));
		c += 1.0/length(vec2(p.x / (sin(i.x+t)/inten),p.y / (cos(1.-time)/inten)));
	}
	c /= float(MAX_ITER);
	c = 1.5-sqrt(c);
	gl_FragColor = vec4(vec3(c*c*c*sqrt(c)*2.), 9999.0) + vec4(.3095, 0.156, 0.05, 1.0);

}