/**
 * \file Serveur.h
 * \brief Gestion du serveur
 * \author J.Cassagne
 */

#ifndef SERVEUR_H
#define SERVEUR_H

#include <iostream>
#include <string>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <fstream>
#include <time.h>

#define PORT 5555

struct Joueur
{
    sf::String nom;
    sf::Color couleur;
    sf::Vector2f position;
    sf::Uint32 score;
    sf::TcpSocket *socket;
    sf::Uint16 animationActuel;
    sf::Uint32 ID;
};

class Serveur
{
    public:
        Serveur(std::string str);
        virtual ~Serveur();
        void Arret();
        bool ChangerMap(std::string str);
        bool SaveScore(std::string suffix = "");
        void Log(std::string, bool);

    private:
        void Reception();
        void AjoutClient(Joueur &joueur, bool nouveau);
        void Transmission(unsigned int index);
        void ActualiserScore(unsigned int index);
        void SuprClient(unsigned int index, bool destruct = false);
        void EcouteNouvelleConnection();

        std::vector <Joueur>tblJoueur;
        std::vector <Joueur>tblNonAuthentifier;
        sf::TcpListener serveur;
        sf::SocketSelector selecteur;
        bool arret;
        std::string nomMap;
        sf::Thread *thread1, *thread2;
        sf::Packet map;

};

#endif // SERVEUR_H
