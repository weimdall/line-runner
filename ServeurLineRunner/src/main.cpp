/**
 * \file main.cpp
 * \brief Boucle serveur
 * \author J.Cassagne
 */

#include "Serveur.h"
#include <unistd.h>

using namespace std;

int main()
{
    Serveur serv("Stage");
    bool arret = false;
    std::string str;
    while(!arret)
    {
        str.clear();
        std::cin >> str;
        if(str == "stop")
        {
            arret = true;
            std::cout << "\nSignal d'arrêt émis ...\n";
            serv.Arret();
        }
        if(str == "map")
        {
            str.clear();
            std::cout << "\nEntrer le nom de la map : ";
            std::cin >> str;
            if(serv.ChangerMap(str))
                serv.Log("\nTransmission de la carte " + str, true);
            else
                std::cout << "\nErreur de chargement.\nLe fichier './data/" << str << ".map' ne semble pas exister." << std::endl;
        }
        if(str == "save")
        {
            str.clear();
            if(serv.SaveScore())
                serv.Log("Scores enregistrés", true);
            else
                serv.Log("Erreur 02", true);
        }

    }
    return 0;
}
