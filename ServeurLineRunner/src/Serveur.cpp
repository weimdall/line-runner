/**
 * \file Serveur.cpp
 * \brief Gestion du serveur
 * \author J.Cassagne
 */

#include "Serveur.h"

Serveur::Serveur(std::string str)
{
    arret = false;
    serveur.setBlocking(true);
    if(serveur.listen(PORT) != sf::Socket::Done)
    {
        Log("Erreur 01", true);
        exit(1);
    }
    else
        Log("Serveur Opérationnel ...", true);


    if(this->ChangerMap(str))
    {
        Log("Map " + str + " chargé.", true);
        thread1 = new sf::Thread(&Serveur::Reception, this);
        thread2 = new sf::Thread(&Serveur::EcouteNouvelleConnection, this);
        thread1->launch();
        thread2->launch();
    }
    else
    {
        Log("\nErreur de chargement.\nLe fichier './data/" + str + ".map' ne semble pas exister.", true);
        Arret();
    }


}

Serveur::~Serveur()
{
    delete thread1;
    delete thread2;
}

void Serveur::EcouteNouvelleConnection()
{
    sf::TcpSocket *tcp;
    do
    {
        tcp = new sf::TcpSocket;
        if (serveur.accept(*tcp) == sf::Socket::Done)
        {
            Log("Tentative de connection", true);
            Joueur nouveau;
            nouveau.score = 0;
            nouveau.position = sf::Vector2f(0,-100);
            nouveau.socket = tcp;
            nouveau.socket->setBlocking(false);
            nouveau.couleur = sf::Color((int)(rand()%255), (int)(rand()%255), (int)(rand()%255));
            selecteur.add(*nouveau.socket);
            tblNonAuthentifier.push_back(nouveau);
        }
    }while(!arret);
}

void Serveur::Reception()
{
    sf::Uint16 type;
    sf::String str;
    sf::Packet paquet;
    sf::Socket::Status status;
    do
    {
        if(selecteur.wait(sf::seconds(1)))
        {
            for(unsigned int i = 0 ; i < tblNonAuthentifier.size() ; i++)
            {
                status = tblNonAuthentifier[i].socket->receive(paquet);
                if(status == sf::Socket::Done)
                {
                    paquet >> type;
                    if(type == 1)
                    {
                        Log("Handshake . ", false);
                        bool nouveau = true;
                        paquet >> str;
                        tblNonAuthentifier[i].nom = str;
                        Log(str+" . ", false);

                        tblNonAuthentifier[i].ID = (tblJoueur.size() > 0) ? tblJoueur[tblJoueur.size()-1].ID +1 : 0;

                        for(unsigned int j = 0 ; j < tblJoueur.size() ; j++)
                        {
                            if(tblJoueur[j].nom.toAnsiString().compare(str.toAnsiString()) == 0)
                            {
                                Log("Recupération infos", true);
                                tblJoueur[j].socket = tblNonAuthentifier[i].socket;
                                nouveau = false;
                                AjoutClient(tblJoueur[j], false);
                                break;
                            }
                        }
                        if(nouveau)
                        {
                            Log("Nouveau", true);
                            AjoutClient(tblNonAuthentifier[i], true);
                            tblJoueur.push_back(tblNonAuthentifier[i]);
                        }

                        tblNonAuthentifier.erase(tblNonAuthentifier.begin()+i);
                    }
                    break;
                }
                else if(status == sf::Socket::Disconnected)
                {
                    delete tblNonAuthentifier[i].socket;
                    tblNonAuthentifier.erase(tblNonAuthentifier.begin()+i);
                }
            }
            for(unsigned int i = 0 ; i < tblJoueur.size() ; i++)
            {
                if(tblJoueur[i].socket != NULL)
                {

                    status = tblJoueur[i].socket->receive(paquet);
                    if(status == sf::Socket::Done)
                    {
                        paquet >> type;
                        if(type == 2)
                        {
                            SuprClient(i);
                            Log("Deconnection client", true);
                        }
                        else if(type == 3)
                        {
                            sf::Vector2f pos;
                            sf::Uint16 anim;
                            paquet >> pos.x >> pos.y >> anim;
                            tblJoueur[i].position = pos;
                            tblJoueur[i].animationActuel = anim;
                            this->Transmission(i);
                        }
                        else if(type == 4)
                        {
                            int score;
                            paquet >> score;
                            if(score-tblJoueur[i].score > 5)
                                Log("Score incohérent . Triche possible de " + tblJoueur[i].nom.toAnsiString() + ".", true);
                            tblJoueur[i].score = score;
                            this->ActualiserScore(i);
                            this->SaveScore("Temp");
                        }
                        paquet.clear();
                        break;
                    }
                    else if(status == sf::Socket::Disconnected)
                        SuprClient(i);
                }
            }
        }
    }while(!arret);
}

void Serveur::Transmission(unsigned int index)
{

    sf::Packet paquet;
    paquet << sf::Uint16(3) << tblJoueur[index].ID << tblJoueur[index].position.x << tblJoueur[index].position.y;
    paquet << tblJoueur[index].animationActuel;
    for(unsigned int i= 0 ; i < tblJoueur.size() ; i++)
        if(i != index && tblJoueur[i].socket != NULL)
            tblJoueur[i].socket->send(paquet);
}
void Serveur::ActualiserScore(unsigned int index)
{
    sf::Packet paquet;
    paquet << sf::Uint16(4) << tblJoueur[index].ID << tblJoueur[index].score;
    for(unsigned int i= 0 ; i < tblJoueur.size() ; i++)
        if(i != index && tblJoueur[i].socket != NULL)
            tblJoueur[i].socket->send(paquet);
}
void Serveur::AjoutClient(Joueur &joueur, bool nouveau)
{
    Log("Ajout client . ", false);

    sf::Packet paquet0, paquet1, paquet2;
    paquet0 << sf::Uint16(0) << joueur.score;
    paquet1 << sf::Uint16(1) << joueur.ID << joueur.nom << joueur.couleur.r << joueur.couleur.g << joueur.couleur.b << joueur.score;

    joueur.socket->setBlocking(true);
    joueur.socket->send(map);
    Log("Envoi stage . ", false);
    joueur.socket->send(paquet0);
    Log("Envoi confirmation . ", false);
    joueur.socket->setBlocking(false);

    for(unsigned int i= 0 ; i < tblJoueur.size() ; i++)
    {
        paquet2 << sf::Uint16(1) << tblJoueur[i].ID << tblJoueur[i].nom << tblJoueur[i].couleur.r << tblJoueur[i].couleur.g << tblJoueur[i].couleur.b << tblJoueur[i].score;
        if(nouveau && tblJoueur[i].socket != NULL)
            tblJoueur[i].socket->send(paquet1);
        if(tblJoueur[i].ID != joueur.ID)
            joueur.socket->send(paquet2);
        paquet2.clear();
    }
    Log("Transmission joueurs", true);
}

void Serveur::SuprClient(unsigned int index, bool destruct)
{
    Log("Supression client . ", false);

    if(tblJoueur[index].socket != NULL)
    {
        selecteur.remove(*(tblJoueur[index].socket));
        tblJoueur[index].socket->disconnect();
        delete tblJoueur[index].socket;
        tblJoueur[index].socket = NULL;
        Log("Supression du socket . ", false);
    }
    if(destruct || tblJoueur[index].score == 0)
    {
        sf::Packet paquet;
        paquet << sf::Uint16(2) << tblJoueur[index].ID;

        tblJoueur.erase(tblJoueur.begin()+index);
        for(unsigned int i= 0 ; i < tblJoueur.size() ; i++)
        {
            if(tblJoueur[i].socket != NULL)
                tblJoueur[i].socket->send(paquet);
        }
        Log("Supression global (màj client)", true);
    }
}

void Serveur::Arret()
{
    arret = true;
    thread1->wait();
    thread2->terminate();
}

bool Serveur::ChangerMap(std::string str)
{
    std::string nomFichier("./data/"+str+".map");
    std::ifstream file(nomFichier.c_str(), std::ifstream::in);
    if(!file)
        return false;
    this->SaveScore("Temp");
    nomMap = str;

    std::string ligne;
    Log("Lecture du fichier . ", false);
    map.clear();
    map << sf::Uint16(5) << str;
    while(std::getline(file, ligne))
    {
        map << ligne;
    }
    file.close();
    Log("Schéma enregistré", true);
    for(unsigned int i= 0 ; i < tblJoueur.size() ; i++)
    {
        if(tblJoueur[i].socket == NULL)
            this->SuprClient(i, true);
        else
        {
            tblJoueur[i].score = 0;
            tblJoueur[i].socket->setBlocking(true);
            tblJoueur[i].socket->send(map);
            tblJoueur[i].socket->setBlocking(false);
        }

    }
    return true;
}

bool Serveur::SaveScore(std::string suffix)
{
    if(nomMap == "")
        return false;
    std::ofstream file(std::string(nomMap + "Scores" + suffix + ".txt").c_str(), std::ofstream::out | std::ofstream::trunc);
    if(!file)
        return false;

    for(unsigned int i = 0 ; i < tblJoueur.size() ; i++)
        file << i+1 << ". " << tblJoueur[i].nom.toAnsiString() << "\t\t " << tblJoueur[i].score << "\n";
    file.close();

    return true;
}

void Serveur::Log(std::string str, bool ecris)
{
    static std::string ligne("");
    ligne += str;
    std::cout << str;
    if(!ecris)
        return;

    std::cout << std::endl;
    std::ofstream log_file("log_file.txt", std::ios_base::out | std::ios_base::app );
    log_file << time(NULL) << " - "  << ligne << std::endl;
    ligne.clear();
}
